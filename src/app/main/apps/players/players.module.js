(function ()
{
    'use strict';

    angular
        .module('app.players', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.players', {
            url      : '/players',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/apps/players/players.html',
                    controller : 'PlayersController as vm'
                }
            },
            resolve  : {
                Documents: function (msApi)
                {
                    return msApi.resolve('players.documents@get');
                }
            },
            bodyClass: 'players'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/players');

        // Api
        msApiProvider.register('players.documents', ['app/data/players/documents.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('apps.players', {
            title : 'Players',
            icon  : 'icon-folder',
            state : 'app.players',
            weight: 6
        });
    }

})();