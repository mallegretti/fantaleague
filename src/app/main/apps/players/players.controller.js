/* global angular */
(function () {
    'use strict';

    angular
        .module('app.players')
        .controller('PlayersController', PlayersController);

    /** @ngInject */
    function PlayersController($mdSidenav, Documents, api) {
        var vm = this;
        
        vm.players = [];
        vm.searchInfo = {
            'limit'   : 0,
            'page'    : 1,
            'pages'   : 0,
            'results' : 0,
            'skip'    : 0,
            'total'   : 0,
            'inProgress' : false
        };
        
        var labels = [];
        for(var i = 0; i <= 38; i++) {
            labels.push( '' + i );
        }
        
        vm.lineChart = {
            data : {
                labels : labels,
                series : []
            },
            options : {
                high : 15,
                low : 0,
                showPoint : false,
                fullWidth : true,
                chartPadding : {
                    right : 40
                },
                axisX: {
                    showLabel: false,
                    showGrid: false
                }
            }
        };
        
        // init search
        search(1);
        vm.selected = {};
        
        // Data
        vm.accounts = {
            'creapond'    : 'johndoe@creapond.com',
            'withinpixels': 'johndoe@withinpixels.com'
        };
        vm.selectedAccount = 'creapond';
        vm.currentView = 'list';
        vm.showDetails = true;

        // Methods
        vm.select = select;
        vm.toggleDetails = toggleDetails;
        vm.toggleSidenav = toggleSidenav;
        vm.search = search;
        vm.searchNext = searchNext;
        vm.searchPrev = searchPrev;
        
        //////////
        
        /**
         * Search players
         *
         * @param page
         */
        function search(page,cb) {
            vm.searchInfo.inProgress = true;
            api.players.list.get({
                'qry'  : {},
                'page' : page
            },
            // Success
            function (res) {
                console.log(res);
                vm.players = res.docs;
                vm.searchInfo.limit = res.limit;
                vm.searchInfo.page = res.page;
                vm.searchInfo.pages = res.pages;
                vm.searchInfo.results = res.results;
                vm.searchInfo.skip = res.skip;
                vm.searchInfo.total = res.total;
                vm.searchInfo.inProgress = false;
                if( "function" == typeof cb ) cb();
            },
            // Error
            function (res) {
                console.error(res);
            });
        }
        
        /**
         * Search players: next page
         */
        function searchNext() {
            if(vm.searchInfo.page < vm.searchInfo.pages) {
                search( vm.searchInfo.page + 1, function() {
                    vm.selected = {};
                });
            }
        }
        
        /**
         * Search players: previous page
         */
        function searchPrev() {
            if(vm.searchInfo.page > 1) {
                search( vm.searchInfo.page - 1, function() {
                    vm.selected = {};
                });
            }
        }
        
        /**
         * Select an item
         *
         * @param item
         */
        function select(player) {
            vm.selected = player;
            vm.lineChart.data.series = [ player.scores, player.scoresAndBonus ];
        }

        /**
         * Toggle details
         *
         * @param item
         */
        function toggleDetails(item) {
            vm.selected = item;
            toggleSidenav('details-sidenav');
        }

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId) {
            $mdSidenav(sidenavId).toggle();
        }
        
    }
})();