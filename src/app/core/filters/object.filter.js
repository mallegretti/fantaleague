/* global angular */
(function ()
{
    'use strict';

    angular
        .module('app.core')
        .filter('isEmpty', isEmpty);

    /** @ngInject */
    function isEmpty() {
        return function(item) {
            return angular.equals( {} , item );
        }
    }

})();