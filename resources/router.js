/* global __dirname */
// import modules
const Express = require('express');
const Router  = Express.Router();

// import the user model
var Player  = require(__dirname + '/models/Player');
var players = require(__dirname + '/api/players');


// define routes

Router.get('/', function(req, res, next) {
  res.send('API up and running...');
});


Router.post('/players/list', function(req, res) {  players.find(req, res); });
Router.get('/players/update', function(req, res) {  players.update(req, res); });
Router.get('/players/bookmark', function(req, res) {  players.bookmark(req, res); });

Router.post('/test', function(req, res) {
  console.log(req.body);
  
  console.log(' ====================================== ');

  //players.update();
    
  /*
  Player.findOne({ 'idgz' : '01236' }, function(err, player) {
    if(err) throw err;
    console.log( (player == null), typeof(player) );
  });
  */

  /*
  // create a new user
  var newPlayer = new Player({
    'gz_a' : [0,0,0],
    'gz_am' : [1,1,1],
    'gz_es' : [2,2,2],
    'gz_g' : [3,3,3],
    'gz_mm' : [4,4,4],
    'gz_mp' : [5,5,5],
    'gz_mv' : [6,6,6],
    'gz_pg': [7,7,7],
    'gz_r': [8,8,8],
    'gz_rp': [9,9,9],
    'gz_rs': [0,1,2],
    'gz_rt': [1,2,3],
    'idgz' : '01235',
    'name' : 'name1',
    'team' : 'team1',
    'price' : [2,3,4],
    'role' : 'role1',
    'role2' : 'role2',
    'scores' : [3,4,5],
    'scoresAndBonus' : [4,5,6]
  });
  
  newPlayer.save(function(err,res) {
    if(err) throw err;
    
    Player.find({}, function(err, res) {
      if (err) throw err;
      // object of all the users
      //console.log(JSON.stringify(res));
    });
    
  });
  */
  
});

module.exports = Router;
