/* global __dirname */
// import modules
const request = require('request');
const cheerio = require('cheerio');

// import 'Player' model
const Player    = require(__dirname + '/../models/Player');


// ---- ------------------------------------------------
// find

exports.find = function(req, res) {
    
    var limit = 20;
    var page = ( 'undefined' == typeof req.body.page ? 1 : req.body.page );
    
    var qry = req.body.qry;
    
    Player.count( qry, function (err, count) {
        if(err) res.json(err);
        var pages = Math.ceil( count / limit );
        page = ( page > pages ? pages : page );
        var skip = (page-1) * limit;
        var results = (count - skip < limit ? count - skip : limit );
        // get paginated players
        Player.find( qry, {}, { 'skip': skip, 'limit': limit, 'sort': { 'name': 1 } }, function (err, players) {
            if(err) res.json(err);
            res.json({
                docs : players,
                limit : limit,
                page : page,
                pages : pages,
                results : results,
                skip : skip,
                total : count
            });
        });
        
    });
    
};



exports.bookmark = function(req, res) {
    
//    if( 'undefined' == typeof req.body.manager ) return;
    
//    var manager = req.body.manager;
    
    
    Player.findById( '57b0edce70abf49810fd1531', function(err, player) {
       // todo:
       // 1. check manager in starred
       // 2. if added, remove, otherwise add
       // 3. save player
       if(err) res.json(err);
       res.json(player);
    });
    /*
    Player.findOne( { '_id' : '57b0edce70abf49810fd1531' }, function(err, player) {
       // todo:
       // 1. check manager in starred
       // 2. if added, remove, otherwise add
       // 3. save player
       if(err) res.json(err);
       res.json(player);
    });
    */
    
    /*
    Player.count( qry, function (err, count) {
        if(err) res.json(err);
        var pages = Math.ceil( count / limit );
        page = ( page > pages ? pages : page );
        var skip = (page-1) * limit;
        var results = (count - skip < limit ? count - skip : limit );
        // get paginated players
        Player.find( qry, {}, { skip: skip, limit: limit }, function (err, players) {
            if(err) res.json(err);
            res.json({
                docs : players,
                limit : limit,
                page : page,
                pages : pages,
                results : results,
                skip : skip,
                total : count
            });
        });
        
    });
    */
    
};


// ------ ----------------------------------------------
// update

exports.update = function(req, res) {

    // TODO: retrieve current day

    var day = 0;
    var season = '2014-15';
    var isHistorical = true;
    
    // retrieve data from Gazzetta
    var urlQtzGz = 'http://www.gazzetta.it/calcio/fantanews/statistiche/serie-a-' + season + '/';
    request(urlQtzGz, function(err, resp, body) {
        if (err) throw err;
        
        // retriieve players from Gazzetta.it
        var newPlayers = _retrievePlayers(body,day,season,isHistorical);
        
        // loop over players and update/insert
        for(var i in newPlayers) {
            var npl = newPlayers[i];
            if( 'undefined' == typeof npl.idgz ) continue;
            _savePlayer(npl,day,season,isHistorical);
        }
        
        res.json('finish');
        
    });

};


// ----- -----------------------------------------------
// other

function _savePlayer(npl,day,season,isHistorical) {
    // by default search by id,
    var qry = { 'idgz' : npl.idgz };
    // but if you are looking for historical statistics, name is better
    if( isHistorical ) {
        qry = { 'name' : npl.name };
    }
    
    Player.findOne(qry, function(err, player) {
        
            if(err) throw err;

            if(player == null) {
                if(isHistorical) return;
                player = new Player(npl);
            } else {
                if(!isHistorical) {
                    // update team
                    player.team = npl.team;
                    // update price
                    player.price[day] = npl.price[day];
                    // update current stats
                    player.stats.gz_a.current[day]  = npl.stats.gz_a.current[day];
                    player.stats.gz_am.current[day] = npl.stats.gz_am.current[day];
                    player.stats.gz_es.current[day] = npl.stats.gz_es.current[day];
                    player.stats.gz_g.current[day]  = npl.stats.gz_g.current[day];
                    player.stats.gz_mm.current[day] = npl.stats.gz_mm.current[day];
                    player.stats.gz_mp.current[day] = npl.stats.gz_mp.current[day];
                    player.stats.gz_mv.current[day] = npl.stats.gz_mv.current[day];
                    player.stats.gz_pg.current[day] = npl.stats.gz_pg.current[day];
                    player.stats.gz_r.current[day]  = npl.stats.gz_r.current[day];
                    player.stats.gz_rp.current[day] = npl.stats.gz_rp.current[day];
                    player.stats.gz_rs.current[day] = npl.stats.gz_rs.current[day];
                    player.stats.gz_rt.current[day] = npl.stats.gz_rt.current[day];
                    // modified fields
                    player.markModified('team');
                    player.markModified('price');
                }
                // update historical stats
                player.stats.gz_a.historical[season]  = npl.stats.gz_a.historical[season];
                player.stats.gz_am.historical[season] = npl.stats.gz_am.historical[season];
                player.stats.gz_es.historical[season] = npl.stats.gz_es.historical[season];
                player.stats.gz_g.historical[season]  = npl.stats.gz_g.historical[season];
                player.stats.gz_mm.historical[season] = npl.stats.gz_mm.historical[season];
                player.stats.gz_mp.historical[season] = npl.stats.gz_mp.historical[season];
                player.stats.gz_mv.historical[season] = npl.stats.gz_mv.historical[season];
                player.stats.gz_pg.historical[season] = npl.stats.gz_pg.historical[season];
                player.stats.gz_r.historical[season]  = npl.stats.gz_r.historical[season];
                player.stats.gz_rp.historical[season] = npl.stats.gz_rp.historical[season];
                player.stats.gz_rs.historical[season] = npl.stats.gz_rs.historical[season];
                player.stats.gz_rt.historical[season] = npl.stats.gz_rt.historical[season];
                // modified fields
                player.markModified('stats');
            }            
            
            // save player
            player.save(function(err,res) {
                if(err) throw err;
                var operation = Math.floor( ( new Date(res.dt_update).getTime() - new Date(res.dt_create).getTime() ) / 1000 ) < 2 ? 'insert' : 'update';
                console.log( operation, res.idgz, res.name ); // TODO: ADD LOGGER
            });

        });
}

// Retrieve players quotations from Gazzetta.it
function _retrievePlayers(body,day,season,isHistorical) {
    
    var $ = cheerio.load(body);
    var players = [];
    
    $('table.playerStats tr').each(function(i) {
        
        // base player object
        var player = {
            'stats' : {
                'gz_a'  : {
                    'current'    : _zeros(39),
                    'historical' : {}
                },
                'gz_am' : {
                    'current'    : _zeros(39),
                    'historical' : {}
                },
                'gz_es' : {
                    'current'    : _zeros(39),
                    'historical' : {}
                },
                'gz_g'  : {
                    'current'    : _zeros(39),
                    'historical' : {}
                },
                'gz_mm' : {
                    'current'    : _zeros(39),
                    'historical' : {}
                },
                'gz_mp' : {
                    'current'    : _zeros(39),
                    'historical' : {}
                },
                'gz_mv' : {
                    'current'    : _zeros(39),
                    'historical' : {}
                },
                'gz_pg' : {
                    'current'    : _zeros(39),
                    'historical' : {}
                },
                'gz_r'  : {
                    'current'    : _zeros(39),
                    'historical' : {}
                },
                'gz_rp' : {
                    'current'    : _zeros(39),
                    'historical' : {}
                },
                'gz_rs' : {
                    'current'    : _zeros(39),
                    'historical' : {}
                },
                'gz_rt' : {
                    'current'    : _zeros(39),
                    'historical' : {}
                }
            },
            'price' : _zeros(39),
            'scores' : _zeros(39),
            'scoresAndBonus' : _zeros(39)
        };

        $(this).find('td').each(function(j) {

            switch( $(this).attr('class').substr(6) ) {

                case 'giocatore':    // player's id and name
                    var href = $(this).children('a').attr('href');
                    player.idgz = href.substr(href.lastIndexOf('_')+1);
                    player.name = $(this).text().trim().toLowerCase().replace('-',' ');
                    break;

                case 'ruolo':    // roles
                    player.role    = $(this).text().trim().toLowerCase().charAt(0);
                    player.role2 = $(this).text().trim().toLowerCase().replace('t (','').replace(')','');
                    break;

                case 'sqd':    // team
                    player.team = $(this).text().trim().toLowerCase();
                    break;

                case 'q selectedField':    // quotation
                    player.price[day] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    break;
                
                case 'pg':    // Partite Giocate
                    if(!isHistorical) {
                        player.stats.gz_pg.current[day] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    }
                    player.stats.gz_pg.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    break;
                
                case 'g':    // Gol fatti/subiti
                    if(!isHistorical) {
                        player.stats.gz_g.current[day] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    }
                    player.stats.gz_g.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    break;
                
                case 'a':    // Assist
                    if(!isHistorical) {
                        player.stats.gz_a.current[day] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    }
                    player.stats.gz_a.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    break;
                
                case 'am':    // Ammonizioni
                    if(!isHistorical) {
                        player.stats.gz_am.current[day] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    }
                    player.stats.gz_am.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    break;
                
                case 'es':    // Espulsioni
                    if(!isHistorical) {
                        player.stats.gz_es.current[day] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    }
                    player.stats.gz_es.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    break;
                    
                case 'rt':    // Rigori Tirati
                    if(!isHistorical) {
                        player.stats.gz_rt.current[day] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    }
                    player.stats.gz_rt.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    break;
                
                case 'r':    // Rigori realizzati
                    if(!isHistorical) {
                        player.stats.gz_r.current[day] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    }
                    player.stats.gz_r.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    break;
                
                case 'rs':    // Rigori Sbagliati
                    if(!isHistorical) {
                        player.stats.gz_rs.current[day] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    }
                    player.stats.gz_rs.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    break;
                
                case 'rp':    // Rigori Parati
                    if(!isHistorical) {
                        player.stats.gz_rp.current[day] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    }
                    player.stats.gz_rp.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseInt($(this).text().trim()) );
                    break;
                    
                case 'mv':    // Media Voto
                    if(!isHistorical) {
                        player.stats.gz_mv.current[day] = ( $(this).text().trim() == "-" ? 0 : parseFloat($(this).text().trim()) );
                    }
                    player.stats.gz_mv.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseFloat($(this).text().trim()) );
                    break;
                
                case 'mm':    // Media Magic Voto
                    if(!isHistorical) {
                        player.stats.gz_mm.current[day] = ( $(this).text().trim() == "-" ? 0 : parseFloat($(this).text().trim()) );
                    }
                    player.stats.gz_mm.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseFloat($(this).text().trim()) );
                    break;
                
                case 'mp':    // Magic Punti (saldo totale bonus/malus)
                    if(!isHistorical) {
                        player.stats.gz_mp.current[day] = ( $(this).text().trim() == "-" ? 0 : parseFloat($(this).text().trim()) );
                    }
                    player.stats.gz_mp.historical[season] = ( $(this).text().trim() == "-" ? 0 : parseFloat($(this).text().trim()) );
                    break;

            }
            
        });

        players.push(player);

    });

    return players;

} // end _retrievePlayers


// create a zero filled array
function _zeros(n) {
    return new Array(n+1).join("0").split("").map(parseFloat);
} // end _zeros