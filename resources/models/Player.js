// import modules
const Mongoose = require('mongoose');

const Schema = Mongoose.Schema;

// create schema
const schema = new Schema({
    'dt_create': {
        'type' : Date,
        'default' : Date.now
    },
    'dt_update': {
        'type' : Date,
        'default' : Date.now
    },
    'stats' : { 'type': Object },
    'idgz' : {
        'type': String,
        'required': true
    },
    'name' : String,
    'team' : String,
    'price' : [{ 'type': Number }],
    'role' : String,
    'role2' : String,
    'scores' : [{ 'type': Number }],
    'scoresAndBonus' : [{ 'type': Number }],
    'season' : {
        'type' : String,
        'default' : '2016-17'
    },
    'starred' : [{
        'type': Schema.Types.ObjectId,
        'ref': 'Manager'
    }]
});

// on every save, add the date
schema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date().toISOString();
    
    // change the updated_at field to current date
    this.dt_update = currentDate;
    
    // if created_at doesn't exist, add to that field
    if (!this.dt_create) {
        this.dt_create = currentDate;
    }
    
    next();
});


module.exports = Mongoose.model('Player', schema);