// import modules
const Mongoose = require('mongoose');

const Schema = Mongoose.Schema;

// create schema
const schema = new Schema({
  'dt_create': {
    'type' : Date,
    'default' : Date.now
  },
  'dt_update': {
    'type' : Date,
    'default' : Date.now
  },
  'manager' : {
    'required': true,
    'type': Schema.Types.ObjectId,
    'ref': 'Manager'
  },
  'type' : {
    'type': String,
    'default': ''
  },
  'value': {
    'type': Number,
    'required': true
  },
});


// on every save, add the date
schema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date().toISOString();
  
  // change the updated_at field to current date
  this.dt_update = currentDate;
  
  // if created_at doesn't exist, add to that field
  if (!this.dt_create) {
    this.dt_create = currentDate;
  }
  
  next();
});


module.exports = Mongoose.model('Finance', schema);