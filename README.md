# FANTALEAGUE - DOCS




## API

> Type GET
> Path /api 


### Players

#### /list
Retrieve players from database.

> `Type` POST

> `Path` /api/players/list

> `Content-Type` application/json

> `Body` Json object which contains both the query in Mongo style and the desider page (if this number is too big, the last page is returned)

Example of `Body` request:

    {
      "qry": {
        "role"  : { "$in" : ["a", "c"] },
        "role2" : { "$in" : ["a", "c"] },
        "team"  : { "$in" : ["atalanta","bologna"] }
      },
      "page" : 5
    }

Example of result:

    {
      "docs"    : [ ... ]
      "limit"   : 20,
      "page"    : 2,
      "pages"   : 2,
      "results" : 11,
      "skip"    : 20,
      "total"   : 31
    }






## Useful links

 * https://scotch.io/tutorials/using-mongoosejs-in-node-js-and-mongodb-applications
 * https://mlab.com/databases/fantaleague2/collections/players
 * http://zellwk.com/blog/crud-express-mongodb/
 * http://mongoosejs.com/docs/populate.html
 * http://mongoosejs.com/docs/queries.html

### Development useful links

 * Pagination http://stackoverflow.com/questions/5539955/how-to-paginate-with-mongoose-in-node-js
 * Nested population http://stackoverflow.com/questions/19222520/populate-nested-array-in-mongoose

### Markdown documentation

 * https://guides.github.com/features/mastering-markdown/