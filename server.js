/* global __dirname */
// import modules
const BodyParser  = require('body-parser');
const Express     = require('express');
const Favicon     = require('serve-favicon');
const Logger      = require('morgan');
const Mongoose    = require('mongoose');
const Passport    = require('passport');
const Path        = require('path');
const Session     = require('express-session');
// import project modules
const Routes      = require('./resources/router.js');

// init app
const app = Express();
// read data from <form> element
app.use(BodyParser.urlencoded({extended: true}));
app.use(BodyParser.json());
app.use(Express.static(Path.join(__dirname + '/dist')));

// database connection
const dbUsr = 'fantaleague2';
const dbPwd = 'nDC$CQ*AxY^LE7vftGfS6$%Q3H9BtH';
const dbUrl = 'mongodb://' + dbUsr + ':' + dbPwd + '@ds029675.mlab.com:29675/fantaleague2';
console.info(new Date().toISOString(), 'Connecting to database...');
Mongoose.connect(dbUrl, function(err) {
  if (err) throw err;
  console.info(new Date().toISOString(), '...connected!');
  startApplication();
});

function startApplication() {
  // start app
  const port = 3100;
  app.listen(port, function() {
    console.log(new Date().toISOString(), 'listening on ' + port);
  });
  
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
  // serve interface
  app.get('/', function(req, res) {
    res.sendFile(__dirname + '/dist/index.html');
  });
  
  // create api
  app.use('/api', Routes);
  
  // Catch everything else and redirect to /index.html
  // Of course you could send the file's content with fs.readFile to avoid
  // using redirects
  app.all('*', function(req, res) {
    res.redirect(__dirname + '/dist/index.html');
  });
  
  
}